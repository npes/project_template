---
title: '19A ITT1 Project'
subtitle: 'Lecture plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait19, 19A
* Name of lecturer and date of filling in form: NISI, ILES 2019-06-01
* Title of the course, module or project and ECTS: Semester project, 5 ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
 INIT     Week  Content
-------- ------ ---------------------------------------------

---------------------------------------------------------


# General info about the course, module or project

## The student’s learning outcome


## Content

The course is formed around a project, and will include relevant technology from the curriculum. It is intended for cross-topic consolidation.

## Method

The project will be divided into blocks that build upon each other. The idea is to create a system that works from sensor to presentation in the cloud while the students practice project management and learn operational skills.

## Equipment

None specified at this time as it is dependent on the topic chosen.

## Projects with external collaborators  (proportion of students who participated)
None at this time.

## Test form/assessment
The project includes x compulsory elements.

See exam catalogue for details on compulsory elements.

## Other general information
None at this time.
