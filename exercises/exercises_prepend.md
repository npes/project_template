---
title: 'COURSE TITLE'
subtitle: 'COURSE SUB TITLE'
authors: ['LECTURER NAME \<MAIL@ucl.dk\>', 'LECTURER NAME \<MAIL@ucl.dk\>']
main_author: 'LECTURER NAME'
date: \today
email: 'MAIL@ucl.dk'
left-header: \today
right-header: 'COURSE TITLE, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References
